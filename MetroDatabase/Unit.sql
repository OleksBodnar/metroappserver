﻿CREATE TABLE [dbo].[Unit]
(
	[Id] INT IDENTITY(1,1) NOT NULL, 
    [UnitId] VARCHAR(50) NOT NULL, 
    [Available] BIT NULL DEFAULT 1, 
    [LeaseTerm] VARCHAR(MAX) NULL, 
    [LeaseExpirationDate] DATE NULL, 
    [Area] FLOAT NULL, 
    [RentPerMonth] FLOAT NULL, 
    [TargetRentPerMonth] FLOAT NULL, 
    [Discription] VARCHAR(MAX) NULL, 
    [TenantId] INT NULL, 
    CONSTRAINT [PK_Unit] PRIMARY KEY CLUSTERED ([Id] ASC), 
    CONSTRAINT [FK_Unit_ToTenant] FOREIGN KEY ([TenantId]) REFERENCES [Tenant]([Id])
)
