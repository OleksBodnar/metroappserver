﻿CREATE TABLE [dbo].[UnitToPhoto]
(
	[Id] INT IDENTITY(1,1) NOT NULL, 
    [UnitId] INT NULL, 
    [PhotoId] INT NULL, 
    [IsCoverPhoto] BIT NULL DEFAULT 0, 
    CONSTRAINT [PK_UnitToPhoto] PRIMARY KEY CLUSTERED ([Id] ASC), 
    CONSTRAINT [FK_UnitToPhoto_ToUnit] FOREIGN KEY ([UnitId]) REFERENCES [Unit]([Id]), 
    CONSTRAINT [FK_UnitToPhoto_ToUnitPhoto] FOREIGN KEY ([PhotoId]) REFERENCES [UnitPhoto]([Id])
)
