﻿CREATE TABLE [dbo].[Tenant]
(
	[Id] INT IDENTITY(1,1) NOT NULL, 
    [Name] VARCHAR(100) NULL, 
    [Type] VARCHAR(50) NULL, 
    [Discription] VARCHAR(MAX) NULL, 
    [Phone] VARCHAR(50) NULL, 
    [WebsiteUrl] VARCHAR(200) NULL, 
    [HoursOfOperation] VARCHAR(100) NULL, 
    [Facebook] VARCHAR(100) NULL, 
    [Instagram] VARCHAR(100) NULL, 
    [CoverImage] VARBINARY(MAX) NULL, 
    CONSTRAINT [PK_Tenant] PRIMARY KEY CLUSTERED ([Id] ASC)	

)
