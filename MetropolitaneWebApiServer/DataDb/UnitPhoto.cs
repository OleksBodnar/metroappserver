﻿using System;
using System.Collections.Generic;

namespace MetropolitaneWebApiServer.DataDb
{
    public partial class UnitPhoto
    {
       

        public int Id { get; set; }
        public string PhotoUrl { get; set; }
        public byte[] UnPhoto { get; set; }

        public ICollection<UnitToPhoto> UnitToPhoto { get; set; }
    }
}
