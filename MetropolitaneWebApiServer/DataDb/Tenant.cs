﻿using System;
using System.Collections.Generic;

namespace MetropolitaneWebApiServer.DataDb
{
    public partial class Tenant
    {
       
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Discription { get; set; }
        public string Phone { get; set; }
        public string WebsiteUrl { get; set; }
        public string HoursOfOperation { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public byte[] CoverImage { get; set; }

        public ICollection<Unit> Unit { get; set; }
    }
}
