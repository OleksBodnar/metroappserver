﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace MetropolitaneWebApiServer.DataDb
{
    public partial class MetroDatabaseContext : DbContext
    {
        public MetroDatabaseContext()
        {
        }

        public MetroDatabaseContext(DbContextOptions<MetroDatabaseContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<Tenant> Tenant { get; set; }
        public DbSet<Unit> Unit { get; set; }
        public DbSet<UnitPhoto> UnitPhoto { get; set; }
        public DbSet<UnitToPhoto> UnitToPhoto { get; set; }       

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Tenant>(entity =>
        //    {
        //        entity.Property(e => e.Discription).IsUnicode(false);

        //        entity.Property(e => e.Facebook)
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.HoursOfOperation)
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Instagram)
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Name)
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Phone)
        //            .HasMaxLength(50)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Type)
        //            .HasMaxLength(50)
        //            .IsUnicode(false);

        //        entity.Property(e => e.WebsiteUrl)
        //            .HasMaxLength(200)
        //            .IsUnicode(false);
        //    });

        //    modelBuilder.Entity<Unit>(entity =>
        //    {
        //        entity.Property(e => e.Available).HasDefaultValueSql("((1))");

        //        entity.Property(e => e.Discription).IsUnicode(false);

        //        entity.Property(e => e.LeaseExpirationDate).HasColumnType("date");

        //        entity.Property(e => e.LeaseTerm).IsUnicode(false);

        //        entity.Property(e => e.UnitId)
        //            .IsRequired()
        //            .HasMaxLength(50)
        //            .IsUnicode(false);

        //        entity.HasOne(d => d.Tenant)
        //            .WithMany(p => p.Unit)
        //            .HasForeignKey(d => d.TenantId)
        //            .HasConstraintName("FK_Unit_ToTenant");
        //    });

        //    modelBuilder.Entity<UnitPhoto>(entity =>
        //    {
        //        entity.Property(e => e.PhotoUrl)
        //            .HasMaxLength(200)
        //            .IsUnicode(false);
        //    });

        //    modelBuilder.Entity<UnitToPhoto>(entity =>
        //    {
        //        entity.Property(e => e.IsCoverPhoto).HasDefaultValueSql("((0))");

        //        entity.HasOne(d => d.Photo)
        //            .WithMany(p => p.UnitToPhoto)
        //            .HasForeignKey(d => d.PhotoId)
        //            .HasConstraintName("FK_UnitToPhoto_ToUnitPhoto");

        //        entity.HasOne(d => d.Unit)
        //            .WithMany(p => p.UnitToPhoto)
        //            .HasForeignKey(d => d.UnitId)
        //            .HasConstraintName("FK_UnitToPhoto_ToUnit");
        //    });
        //}
    }
}
