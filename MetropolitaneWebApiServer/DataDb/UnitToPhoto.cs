﻿using System;
using System.Collections.Generic;

namespace MetropolitaneWebApiServer.DataDb
{
    public partial class UnitToPhoto
    {
        public int Id { get; set; }
        public int? UnitId { get; set; }
        public int? PhotoId { get; set; }
        public bool? IsCoverPhoto { get; set; }

        public UnitPhoto Photo { get; set; }
        public Unit Unit { get; set; }
    }
}
