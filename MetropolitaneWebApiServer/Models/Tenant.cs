﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MetropolitaneWebApiServer.Models
{
    public class Tenant
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Discription { get; set; }
        public string Phone { get; set; }
        public string WebsiteUrl { get; set; }
        public string HoursOfOperation { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public byte[] CoverImage { get; set; }

    }
}
