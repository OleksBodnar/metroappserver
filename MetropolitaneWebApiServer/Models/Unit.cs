﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace MetropolitaneWebApiServer.Models
{
    public class Unit
    {
        public int Id { get; set; }
        public string UnitId { get; set; }
        public bool? Available { get; set; }
        public string LeaseTerm { get; set; }
        public DateTime? LeaseExpirationDate { get; set; }
        public double? Area { get; set; }
        public double? RentPerMonth { get; set; }
        public double? TargetRentPerMonth { get; set; }
        public string Discription { get; set; }
        public int? TenantId { get; set; }

        public Tenant Tenant { get; set; }
        public ICollection<UnitToPhoto> UnitToPhoto { get; set; }
    }
}
