﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MetropolitaneWebApiServer.Models
{
    public class UnitToPhoto
    {
        public int UnitId { get; set; }
        public int PhotoId { get; set; }
    }
}
