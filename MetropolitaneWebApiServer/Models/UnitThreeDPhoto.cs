﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MetropolitaneWebApiServer.Models
{
    public class UnitThreeDPhoto
    {
        public int Id { get; set; }
        public string PhotoTrDUrl { get; set; }
        public byte[] UnittrDPhoto { get; set; }
    }
}
