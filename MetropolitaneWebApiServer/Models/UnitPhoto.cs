﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MetropolitaneWebApiServer.Models
{
    public class UnitPhoto
    {
        public int Id { get; set; }
        public string PhotoUrl { get; set; }
        public byte[] UnPhoto { get; set; }
    }
}
