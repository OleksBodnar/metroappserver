﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MetropolitaneWebApiServer.DataDb;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MetropolitaneWebApiServer.Controllers
{
    
    public class BaseController : Controller
    {
        public MetroDatabaseContext db;
        public BaseController(MetroDatabaseContext context)
        {
            db = context;
        }
    }
}