﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MetropolitaneWebApiServer.DataDb;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MetropolitaneWebApiServer.Controllers
{
    
    public class HomeController : BaseController
    {
        public HomeController(MetroDatabaseContext context) : base(context)
        { }

        public async Task<IActionResult> Index()
        {
            return View(await db.Unit.ToListAsync());
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Unit model)
        {
            db.Unit.Add(model);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Edit(int? id)
        {
            Unit unit = await db.Unit.FirstOrDefaultAsync(u => u.Id == id);
            return View(unit);

        }

        [HttpPost]
        public async Task<IActionResult> Edit(Unit model)
        {
            db.Unit.Update(model);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }


    }
}